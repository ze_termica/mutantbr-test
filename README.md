# Mauricio's Silvestre Mutant test

## Test overview
### 
![Blueprint](blueprint.png)

## Instructions to run

### Clone the repository
git clone https://ze_termica@bitbucket.org/ze_termica/mutantbr-test.git

### Go to mutantbr-test
comand: cd mutantbr-test

### Run the host VM
comand: vagrant up

### How to test from your machine
Open a browser and go to http://192.168.50.4

### How to verify Kibana
Open a browser and go to http://192.168.50.4:5601

### Generated APIs on node server:

#### GET /site => Return a list of all user's website 
#### GET /company => Return name, email and company sorted
#### GET /suites  =>  Return the Users that have 'Suite' world in their address

### All interactions may be checked in Elasticsearch or node-server docker output

###  Postman API refs
https://documenter.getpostman.com/view/1452754/T1LHH9ju

### All interactions are being stored by a POST request in a "collection" called an interaction.

## Repositories

### PWA Sources 
git clone https://ze_termica@bitbucket.org/ze_termica/ionic-front.git

### Node server sources
git clone https://ze_termica@bitbucket.org/ze_termica/node-server.git

## 
![Donate](https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif)]
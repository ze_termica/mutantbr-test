FROM node:latest
LABEL MAINTAINER Mauricio Silvestre

ENV PORT=3000
ENV NODE_ENV=development

RUN git clone https://ze_termica@bitbucket.org/ze_termica/node-server.git
WORKDIR /node-server
RUN npm install
ENTRYPOINT npm start
EXPOSE 3000